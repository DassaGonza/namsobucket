/*
 * DAO de objeto
 */
package bitacorared;

/**
 *
 * @author Namso
 */
public class bitacoraDAO {
    private String NOMBRERED;
    private String ID;
    private String MAC;
    private String ADMIN;
    private String IP;
    private String RECURSOS;
    private String PUERTOS;
    private String PROXY;
    private String GATEWAY;
    private String DNS;
    private String RESPONSABLE;
    private String FIRMA;

    public String getNOMBRERED() {
        return NOMBRERED;
    }

    public void setNOMBRERED(String NOMBRERED) {
        this.NOMBRERED = NOMBRERED;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getMAC() {
        return MAC;
    }

    public void setMAC(String MAC) {
        this.MAC = MAC;
    }

    public String getADMIN() {
        return ADMIN;
    }

    public void setADMIN(String ADMIN) {
        this.ADMIN = ADMIN;
    }

    public String getRECURSOS() {
        return RECURSOS;
    }

    public void setRECURSOS(String RECURSOS) {
        this.RECURSOS = RECURSOS;
    }

    public String getPROXY() {
        return PROXY;
    }

    public void setPROXY(String PROXY) {
        this.PROXY = PROXY;
    }

    public String getGATEWAY() {
        return GATEWAY;
    }

    public void setGATEWAY(String GATEWAY) {
        this.GATEWAY = GATEWAY;
    }

    public String getDNS() {
        return DNS;
    }

    public void setDNS(String DNS) {
        this.DNS = DNS;
    }

    public String getRESPONSABLE() {
        return RESPONSABLE;
    }

    public void setRESPONSABLE(String RESPONSABLE) {
        this.RESPONSABLE = RESPONSABLE;
    }

    public String getFIRMA() {
        return FIRMA;
    }

    public void setFIRMA(String FIRMA) {
        this.FIRMA = FIRMA;
    }

    public String getIP() {
        return IP;
    }

    public void setIP(String IP) {
        this.IP = IP;
    }

    public String getPUERTOS() {
        return PUERTOS;
    }

    public void setPUERTOS(String PUERTOS) {
        this.PUERTOS = PUERTOS;
    }
    
    
}
