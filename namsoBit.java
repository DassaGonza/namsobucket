/*
 * Esta clase cifra y desifra cadenas de texto bajo una llave 
 */
package bitacorared;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Random;

/**
 *
 * @author Osman Dassaed González Ibarra
 */
public class namsoBit {
    private int nivelEncriptation;
    private String key;
    
    public namsoBit(){
        this.key="Namso?!@#$%458L";
        this.nivelEncriptation=3;
    }
    
    /**
     * Encripta una cadena de caracteres con una llave generica
     * @param g
     * @return 
     */
    public String encriptar(String g){
        return encrypt(g, key);
    }
    
    /**
     * Encripta una cadena de caracteres con una llave personalizada
     * @param g
     * @param k
     * @return 
     */
    public String encriptar(String g, String k){
        return encrypt(g, k);
    }
    
    /**
     * Desencripta una cadena de caracteres con una llave generica
     * @param g
     * @return 
     */
    public String desencriptar(String g){
        return decrypt(g, key);
    }
    
    /**
     * Desencripta una cadena de caracteres con una llave personalizada
     * @param g
     * @param k
     * @return 
     */
    public String desencriptar(String g, String k){
        return decrypt(g, k);
    }
    
    /**
     * Encripta una cadena de caracteres
     * @param s
     * @param key
     * @return 
     */
    private String encrypt(String s, String key){
        if(s.equals(""))
            return "";
	String r = "";
	for(int i=0; i<s.length(); i++) {
            Random rnd = new Random(key.length()+s.length()+i);
            String c = s.substring(i, i+1);
            int in = (i%(key.length()-1));
            String kc = key.substring(in, in+1);
            int cc = c.codePointAt(0) + kc.codePointAt(0) + rnd.nextInt(nivelEncriptation);
            r+=(char)cc;
	}
	return Base64.getEncoder().encodeToString(r.getBytes(StandardCharsets.UTF_8));
    }
    
    /**
     * Desencripta una cadena de caracteres
     * @param s
     * @param key
     * @return 
     */
    private String decrypt(String s, String key) {
        if(s.equals(""))
            return "";
	String r = "";
        byte[] decodedBytes = Base64.getDecoder().decode(s.getBytes(StandardCharsets.UTF_8));
	s = new String(decodedBytes);
	for(int i=0; i<s.length(); i++) {
            Random rnd = new Random(key.length()+s.length()+i);
            String c = s.substring(i, i+1);
            int in = (i%(key.length()-1));
            String kc = key.substring(in, in+1);
            int cc = c.codePointAt(0) - kc.codePointAt(0) - rnd.nextInt(nivelEncriptation);
            r+=(char)cc;
	}
	return r;
    }

    /**
     * Devuelve el nivel de encriptacion
     * @return 
     */
    public int getNivelEncriptation() {
        return nivelEncriptation;
    }

    /**
     * Establece el nivel de encrptacion
     * @param nivelEncriptation 
     */
    public void setNivelEncriptation(int nivelEncriptation) {
        this.nivelEncriptation = nivelEncriptation;
    }

    /**
     * Establecer la llave por defecto
     * @param key 
     */
    public void setKey(String key) {
        this.key = key;
    }
    
    /**
     * Devuelve un MD5 de un string
     * @param input
     * @return 
     */
    public String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        catch (NoSuchAlgorithmException e) {
            return "";
        }
    }
}
