package bitacorared;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author Namso
 */
public class namsoBucket {
    private String dbFolder = "db";
    private String db = "resources.ck";
    private ArrayList<frame> frames;
    private namsoBit nb = new namsoBit();
    private int masterId=0;
    
    public namsoBucket(){
        frames = new ArrayList<>();
        nb.setKey("Namso?!@#$%458L");
        //Preparamos nuestro archivo que recibira 
        while(!loadResources()){
            try {
                File folder = new File(dbFolder);
                if(!folder.exists()){
                    folder.mkdirs();
                    System.out.println("[INFO] Folders creados con éxito...");
                }

                File file1 = new File (db);
                if(!file1.exists()){
                    file1.createNewFile();
                    System.out.println("[INFO] Checksum Validator creado");
                }                
            } catch (IOException e) {
                System.out.println("[ERROR] No se pudo crear el archivo, saliendo...");
                return;
            }
        }
        System.out.println("[INFO] namsoBucket en linea!");
    }
    
    private boolean loadResources(){
        try {
            File archivo = new File(dbFolder+"/"+db);
            FileReader fr = new FileReader(archivo);
            BufferedReader br = new BufferedReader(fr);
            
            String linea;
            while((linea=br.readLine())!=null){
                //System.out.println(nb.desencriptar(linea));
                String[] g = nb.desencriptar(linea).split(",");
                frames.add(new frame(Integer.parseInt(g[0]), g[1]));
            }
            fr.close();
            verifyIntegrity();
            return true;
        }catch(IOException e){
            return false;
        }
    }
    
    private void verifyIntegrity(){
        for(frame f : frames){
            File ar = new File(dbFolder+"/"+nb.getMD5(""+f.getId())+".ns");
            //System.out.println(f.getChecksum()+"=="+getCheckSum(ar));
            if(ar.exists() && f.getChecksum().equals(getCheckSum(ar))) {
                masterId=(f.getId()>masterId)? f.getId() : masterId;
            } else
                frames.remove(f);
        }
        
        //Reindexamos
        reindex();
    }
    
    private void reindex(){
        ArrayList<frame> framesAux=frames;
        frames = new ArrayList<>();
        for(frame f : framesAux)
            frames.add(f);
    }
    
    private String getCheckSum(File ar){
        try{
            FileReader fr = new FileReader(ar);
            BufferedReader br = new BufferedReader(fr);
            long sum=0;
            String linea;
            while((linea=br.readLine())!=null){
                String[] ln = nb.desencriptar(linea).split("");
                for(String h : ln)
                    sum+=h.codePointAt(0);
            }
            fr.close();
            
            return nb.getMD5(""+sum);
        }catch(IOException e){
            return "";
        }
    }
    
    public ArrayList<bitacoraDAO> getAll(){
        ArrayList<bitacoraDAO> q = new ArrayList<>();
        for(frame f : frames){
            bitacoraDAO o = new bitacoraDAO();
            ArrayList<String> buffer = new ArrayList<>();
            try{
                FileReader fr = new FileReader(dbFolder+"/"+nb.getMD5(""+f.getId())+".ns");
                BufferedReader br = new BufferedReader(fr);
                
                String linea;
                while((linea=br.readLine())!=null){
                    buffer.add(nb.desencriptar(linea));
                }
                fr.close();
            }catch(IOException e){
                buffer.add("");
            }
            
            o.setNOMBRERED(buffer.get(0));
            o.setID(buffer.get(1));
            o.setMAC(buffer.get(2));
            o.setIP(buffer.get(3));
            o.setADMIN(buffer.get(4));
            o.setRECURSOS(buffer.get(5));
            o.setPUERTOS(buffer.get(6));
            o.setPROXY(buffer.get(7));
            o.setGATEWAY(buffer.get(8));
            o.setDNS(buffer.get(9));
            o.setRESPONSABLE(buffer.get(10));
            o.setFIRMA(buffer.get(11));
            
            q.add(o);
        }
        return q;
    }
    
    public bitacoraDAO getRow(int id){
        if(!issetID(id)){
            System.out.println("[namsoBucket] El id no esta registrado en la DB");
            return null;
        }
        
        bitacoraDAO o = new bitacoraDAO();
        ArrayList<String> buffer = new ArrayList<>();
        try{
            FileReader fr = new FileReader(dbFolder+"/"+nb.getMD5(""+id)+".ns");
            BufferedReader br = new BufferedReader(fr);

            String linea;
            while((linea=br.readLine())!=null){
                buffer.add(nb.desencriptar(linea));
            }
            fr.close();
        }catch(IOException e){
            buffer.add("");
        }

        o.setNOMBRERED(buffer.get(0));
        o.setID(buffer.get(1));
        o.setMAC(buffer.get(2));
        o.setIP(buffer.get(3));
        o.setADMIN(buffer.get(4));
        o.setRECURSOS(buffer.get(5));
        o.setPUERTOS(buffer.get(6));
        o.setPROXY(buffer.get(7));
        o.setGATEWAY(buffer.get(8));
        o.setDNS(buffer.get(9));
        o.setRESPONSABLE(buffer.get(10));
        o.setFIRMA(buffer.get(11));
        
        return o;
    }
    
    private boolean issetID(int id){
        for(frame f : frames){
            if(f.getId()==id)
                return true;
        }
        return false;
    }
    
    public boolean delete(int id){
        if(!issetID(id)){
            System.out.println("[namsoBucket] El id no esta registrado en la DB");
            return false;
        }
        deleteRow(id);
        return true;
    }
    
    public boolean delete(int id, boolean deep){
        if(!issetID(id)){
            System.out.println("[namsoBucket] El id no esta registrado en la DB");
            return false;
        }
        if(deep){
            File row = new File(dbFolder+"/"+nb.getMD5(""+id)+".ns");
            if(row.exists())
                row.delete();
        }
        deleteRow(id);
        return true;
    }
    
    private void deleteRow(int id){
        for(frame f : frames){
            if(f.getId()==id)
                frames.remove(f);
        }
        reindex();
        putUpdate();
    }
    
    private void putUpdate(){
        try{
            
            FileWriter fi = new FileWriter(dbFolder+"/"+db);
            PrintWriter pw = new PrintWriter(fi);
            
            for(frame f : frames)
                pw.println(nb.encriptar(f.getId()+","+f.getChecksum()));                           

            fi.close();
        } catch (IOException e) {
            System.out.println("[ERROR FATAL]");
        }
    }
    
    public long insert(bitacoraDAO g){
        int id=Integer.parseInt(g.getID());
        //Agremamos logicamente el registro
        masterId=(id>masterId)? id : masterId; 
        frames.add(new frame(masterId,""));
        
        //Creamos el archivo
        try{
            File row = new File(dbFolder+"/"+nb.getMD5(""+id)+".ns");
            if(row.createNewFile()){
                try{
                    FileWriter f = new FileWriter(dbFolder+"/"+nb.getMD5(""+id)+".ns");
                    PrintWriter pw = new PrintWriter(f);

                    pw.println(nb.encriptar(g.getNOMBRERED()));
                    pw.println(nb.encriptar(g.getID()));
                    pw.println(nb.encriptar(g.getMAC()));
                    pw.println(nb.encriptar(g.getIP()));
                    pw.println(nb.encriptar(g.getADMIN()));
                    pw.println(nb.encriptar(g.getRECURSOS()));
                    pw.println(nb.encriptar(g.getPUERTOS()));
                    pw.println(nb.encriptar(g.getPROXY()));
                    pw.println(nb.encriptar(g.getGATEWAY()));
                    pw.println(nb.encriptar(g.getDNS()));
                    pw.println(nb.encriptar(g.getRESPONSABLE()));  
                    pw.println(nb.encriptar(g.getFIRMA()));                  
                    
                    f.close();
                } catch (IOException e) {
                    return -1;
                }
                
                frames.get(frames.size()-1).setChecksum(getCheckSum(row));
            }
        }catch(IOException e){
            return -1;
        }        
        
        //Actualizamos el archivo de lotes
        putUpdate();
        return masterId;
    }
    
    public boolean update(bitacoraDAO g){
        int id = Integer.parseInt(g.getID());
        if(!issetID(id)){
            System.out.println("[namsoBucket] El id no esta registrado en la DB");
            return false;
        }
        try{
            FileWriter f = new FileWriter(dbFolder+"/"+nb.getMD5(""+id)+".ns");
            PrintWriter pw = new PrintWriter(f);

            pw.println(nb.encriptar(g.getNOMBRERED()));
            pw.println(nb.encriptar(g.getID()));
            pw.println(nb.encriptar(g.getMAC()));
            pw.println(nb.encriptar(g.getIP()));
            pw.println(nb.encriptar(g.getADMIN()));
            pw.println(nb.encriptar(g.getRECURSOS()));
            pw.println(nb.encriptar(g.getPUERTOS()));
            pw.println(nb.encriptar(g.getPROXY()));
            pw.println(nb.encriptar(g.getGATEWAY()));
            pw.println(nb.encriptar(g.getDNS()));
            pw.println(nb.encriptar(g.getRESPONSABLE()));    
            pw.println(nb.encriptar(g.getFIRMA()));                  

            f.close();
            
            File row = new File(dbFolder+"/"+nb.getMD5(""+id)+".ns");
            for(frame t : frames){
                if(t.getId()==id)
                    t.setChecksum(getCheckSum(row));
            }
        } catch (IOException e) {
            return false;
        }
        
        //Actualizamos el archivo de lotes
        putUpdate();
        return true;
    }

    public int getMasterId() {
        return masterId;
    }

    public void setMasterId(int masterId) {
        this.masterId = masterId;
    }
}
